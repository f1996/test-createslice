import { useSelector } from "react-redux"

const AffichageNombre2 = () => {
    const leNombre2 = useSelector(state => state.nombre2.leNombre2)

    return (
        <>
            <p>le nombre2 égale {leNombre2}</p>
        </>
    )
}

export default AffichageNombre2