import { useSelector } from "react-redux"

const AffichageNombre = () => {
    const leNombre = useSelector(state => state.nombre.leNombre)

    return (
        <>
            <p>le nombre égale {leNombre}</p>
        </>
    )
}

export default AffichageNombre