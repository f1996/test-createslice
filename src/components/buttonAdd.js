import { useDispatch } from "react-redux"
import { addition } from "../feature/leNombreSlice"


const ButtonAdd = () => {
    const dispatch = useDispatch()

    const handleClick = (value) => {
        dispatch(addition(value))
    }


    return (
        <>
            <button onClick={() => handleClick(1)}>+1</button>
        </>
    )
}

export default ButtonAdd