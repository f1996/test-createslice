import { useDispatch } from "react-redux"
import { addition2 } from "../feature/leNombre2Slice"


const ButtonAdd2 = () => {
    const dispatch = useDispatch()

    const handleClick = (value) => {
        dispatch(addition2(value))
    }


    return (
        <>
            <button onClick={() => handleClick(2)}>+2</button>
        </>
    )
}

export default ButtonAdd2