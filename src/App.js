
import './App.css';
import AffichageNombre from './components/affichageNombre';
import AffichageNombre2 from './components/affichageNombre2';
import ButtonAdd from './components/buttonAdd';
import ButtonAdd2 from './components/buttonAdd2';

function App() {
  return (
    <div className="App">
      <AffichageNombre/>
      <ButtonAdd/>
      <AffichageNombre2/>
      <ButtonAdd2/>
    </div>
  );
}

export default App;
