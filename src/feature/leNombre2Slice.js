import { createSlice } from "@reduxjs/toolkit";

export const leNombreSlice2 = createSlice({
    name:"nombre2",
    initialState:{
        leNombre2:0
    },
    reducers: {
        addition2:(state, {payload}) => {
            state.leNombre2 = state.leNombre2 + payload
        }
    }
})

export const {addition2} = leNombreSlice2.actions
export default leNombreSlice2.reducer