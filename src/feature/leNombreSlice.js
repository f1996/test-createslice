import { createSlice } from "@reduxjs/toolkit";

export const leNombreSlice = createSlice({
    name:"nombre",
    initialState:{
        leNombre:0
    },
    reducers: {
        addition:(state, {payload}) => {
            state.leNombre = state.leNombre + payload
        }
    }
})

export const {addition} = leNombreSlice.actions
export default leNombreSlice.reducer