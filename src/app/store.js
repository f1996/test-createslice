import { configureStore } from "@reduxjs/toolkit"
import leNombreReducer from "../feature/leNombreSlice"
import leNombre2Reducer  from "../feature/leNombre2Slice"

export default configureStore({
    reducer: {
        nombre:leNombreReducer,
        nombre2:leNombre2Reducer
    }
})